
<h1 align="center">Rest Countries API</h1>

## Table of Contents

- [Overview](#overview)
- [Project Link](#project-link)
- [Built With](#built-with)
- [How to use](#how-to-use)
- [Contact](#contact)

## Overview

<a href="" rel="image text"><img src="public/rest-countriesapi.png" alt="" /></a>

- Display Country details
- Search based on Country name
- Filter based on region

## Project Link

<a href="https://rekha-sj-rest-countries-api.netlify.app/" >Click To Check Project Link</a>

### Built With

- HTML
- CSS
- Bootstrap
- React JS

## How To Use

To clone and run this application, you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
$ git clone git@gitlab.com:Rekhasj21/rest-countries-api.git

# Install dependencies
$ npm install

# Run the app
$ npm start
```

## Contact

- Website (<https://rekha-sj-rest-countries-api.netlify.app/>)
- GitHub (<https://gitlab.com/Rekhasj21/rest-countries-api>)