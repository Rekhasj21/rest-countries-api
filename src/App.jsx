import { useState } from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import HomePage from './components/HomePage'
import CountryDetails from './components/CountryDetails'
import Navbar from './components/Navbar'
import './App.css'

export default function App() {
    const [colorStatus, setColorStatus] = useState(false)

    function handleBackgroundColor() {
        setColorStatus(prevcolorStatus => !prevcolorStatus)
    }

    return (
        <BrowserRouter>
            <Navbar onClickColorChange={handleBackgroundColor} colorStatus={colorStatus} />
            <Routes>
                <Route path="/" element={<HomePage colorStatus={colorStatus} />} />
                <Route path="/:capitalname" element={<CountryDetails />} />
            </Routes>

        </BrowserRouter>
    )
}
