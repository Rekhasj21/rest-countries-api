import { Link } from "react-router-dom"

export default function DisplayPage({ countryData }) {
      const { name, population, region, capital, flags, subregion, area,cca3 } = countryData

    return (
        <Link className="link" to={`${capital}`} >
            <div className="w-20 shadow-sm m-5 bg-body-tertiary rounded ">
                <img src={flags.png} alt={name.common} className='w-100' />
                <h1 className="fs-5 heading">{name.common}</h1>
                <ul className="d-flex flex-column justify-content-start">
                    <li className="list-group-item">Population:<span>{population}</span></li>
                    <li className="list-group-item">Region: <span>{region}</span></li>
                    <li className="list-group-item">Sub-Region: <span>{subregion}</span></li>
                    <li className="list-group-item">Capital:<span>{capital} </span></li>
                    <li className="list-group-item">Area:<span>{area} </span></li>
                </ul>
            </div>
         </Link>
    )
}