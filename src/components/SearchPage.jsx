export default function Search({ handleSearch }) {

    return (

        <div className="w-10 shadow p-3 m-5 bg-body-tertiary rounded">
            <img className="search-icon" src='/search-26242.png' />
            <input type="text" placeholder="Search by name" className="user-input w-75" onChange={handleSearch} />
        </div>

    )
}