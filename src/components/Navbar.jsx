export default function Navbar({ onClickColorChange, colorStatus }) {
    function handleBackgroundColor() {
        onClickColorChange()
    }
    const imageUrl = colorStatus ? "/dark-mode-icon.png" : '/light-mode-icon.png'

    return (
        <nav className="navbar p-4 shadow-sm m-1 bg-body-tertiary rounded" style={{
            backgroundColor: colorStatus ? 'black' : 'white'
        }}>
            <h1 style={{
                color: colorStatus ? 'white' : 'black'
            }}>Where in the world?</h1>
            <button type='button' className="border border-0 button" onClick={() => handleBackgroundColor()}>
                <img src={imageUrl} className="mode-icon" placeholder="dark mode" />
                Dark Mode</button>
        </nav>
    )
}