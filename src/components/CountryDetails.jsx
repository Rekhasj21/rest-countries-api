import React, { useState, useEffect } from 'react'
import { ColorRing } from 'react-loader-spinner'
import { Link, useParams } from 'react-router-dom'

export default function CountryDetails() {
    const [countryData, setCountryData] = useState(null);
    const { capitalname } = useParams()

    const getCountryData = async () => {
        const response = await fetch(`https://restcountries.com/v3.1/capital/${capitalname}`);
        const data = await response.json();
        setCountryData(data[0]);
    };

    useEffect(() => {
        getCountryData();
    }, []);

    if (!countryData) {
        return <div className='text-center'>
            <ColorRing className='text-center' visible={true} height="80" width="80" ariaLabel="blocks-loading" wrapperStyle={{}} wrapperClass="blocks-wrapper"
                colors={['#e15b64', '#f47e60', '#f8b26a', '#abbd81', '#849b87']} />
        </div>;
    }
    const { flags, name, population, region, subregion, capital, languages, tld } = countryData;

    return (
        <div>
            <Link to="/">
                <button className="btn btn-primary m-5">Back</button>
            </Link>

            <div className="d-flex justify-content-evenly">
                <img src={flags.png} alt={name.common} className="w-50" />

                <div>
                    <h1>{name.common}</h1>
                    <div className="d-flex">
                        <ul className="list-group list-group-vertical p-4">
                            <li className="list-group-item">Native Name: {[Object.keys(name.nativeName)[0]]?.common || 'N/A'}</li>
                            <li className="list-group-item">Population: {population}</li>
                            <li className="list-group-item">Region: {region}</li>
                            <li className="list-group-item">Sub Region: {subregion}</li>
                        </ul>
                        <ul className="list-group list-group-vertical p-4">
                            <li className="list-group-item">Top Level Domain: {tld?.[0] || "N/A"}</li>
                            <li className="list-group-item">Currencies: {Object.values(countryData.currencies)?.[0]?.name || "N/A"}</li>
                            <li className="list-group-item">Languages: {Object.values(languages)?.[0] || "N/A"}</li>
                        </ul>
                    </div>
                    <div className="d-flex align-items-center">
                        <h1 className="fs-5">Border Countries:</h1>
                        <ul className="list-group list-group-horizontal p-4">
                            {countryData.borders?.length ? countryData.borders.map(border => <li className="list-group-item" key={border}>{border}</li>) : <li className="list-group-item">N/A</li>}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}