import { v4 as uuidv4 } from "uuid"

export default function CountryFilterSortPage({ handleRegionChange, filterList, optionValue, selectedValue }) {

    function onChangeOption(event) {
        handleRegionChange(event.target.value)
    }

    return (
        <div className="filter m-3">
            <select className="form-select option" value={selectedValue} aria-label="Floating label select example" onChange={(event) => onChangeOption(event)}>
                <option key={uuidv4()} value="All">{optionValue}</option>
                {filterList.map(filter => <option key={uuidv4()} value={filter} >{filter}</option>)}
            </select>
        </div>
    )
}