import React, { useEffect, useState } from 'react'
import { ColorRing } from 'react-loader-spinner'
import DisplayPage from './DisplayPage'
import SearchPage from './SearchPage'
import CountryFilterSortPage from './CountryFilterSortPage'
import "../App.css"

export default function HomePage({ colorStatus }) {
    const [countryList, setCountryList] = useState([])
    const [regionData, setRegionData] = useState("")
    const [subRegionData, setSubRegionData] = useState("")
    const [searchValue, setSearchValue] = useState('')
    const [sortValue, setSortValue] = useState('')
    const [isLoading, setLoading] = useState(true)

    const regions = [...new Set(countryList.map(country => country.region))]
    const sortList = ['name', "area", "population"]

    const getCountriesData = async () => {
        const response = await fetch('https://restcountries.com/v3.1/all')
        const countryData = await response.json()
        setCountryList(countryData)
        setLoading(false)
    }

    useEffect(() => {
        getCountriesData()
    }, [])

    function handleSearch(event) {
        setSearchValue(event.target.value)
    }

    function onSelectRegion(region) {
        setRegionData(region)
        setSubRegionData('')
    }

    function onSelectSubRegion(subRegion) {
        setSubRegionData(subRegion)
    }

    function onClickSortName(sortName) {
        setSortValue(sortName)
    }

    let filteredCountries = [...countryList]
    if (regionData !== '' && regionData !== 'All') {
        filteredCountries = filteredCountries.filter((country) => country.region === regionData);
    }
    if (subRegionData !== '' && subRegionData !== 'All') {
        filteredCountries = filteredCountries.filter((country) => country.subregion === subRegionData);
    }

    searchValue && (filteredCountries = filteredCountries.filter((country) =>
        country.name.common.toLowerCase().includes(searchValue.toLowerCase())))

    sortValue === 'name' && (filteredCountries = [...filteredCountries].sort((a, b) => a.name.common > b.name.common ? 1 : -1))
    sortValue === 'area' && (filteredCountries = [...filteredCountries].sort((a, b) => a.area > b.area ? 1 : -1))
    sortValue === 'population' && (filteredCountries = [...filteredCountries].sort((a, b) => a.population > b.population ? 1 : -1))

    function getCountriesBySubregion(countries, region) {
        const subregions = new Set();
        countries.filter((country) => region === "ALL" || country.region === region)
            .forEach((country) => country.subregion && subregions.add(country.subregion));
        return Array.from(subregions);
    }

    return (
        <>
            <div style={{
                backgroundColor: colorStatus ? 'black' : 'white',
                color: colorStatus ? 'white' : 'black'
            }} >
                <div className='d-flex'>
                    <div className='d-flex flex-column justify-content-start align-items-start w-25 m-3'>
                        <SearchPage handleSearch={handleSearch} />
                        <CountryFilterSortPage handleRegionChange={onSelectRegion} filterList={regions} optionValue="Filter by Region" selectedValue={regionData} />
                        <CountryFilterSortPage handleRegionChange={onSelectSubRegion} filterList={getCountriesBySubregion(countryList, regionData)} optionValue="Filter by Sub-Region" selectedValue={subRegionData} />
                        <CountryFilterSortPage handleRegionChange={onClickSortName} filterList={sortList} optionValue="Sort By" selectedValue={sortValue} />
                    </div>
                    <div className='d-flex flex-wrap w-75'>
                        {isLoading ? (<ColorRing className='text-center' visible={true} height="80" width="80" ariaLabel="blocks-loading"
                            wrapperStyle={{}} wrapperClass="blocks-wrapper" colors={['#e15b64', '#f47e60', '#f8b26a', '#abbd81', '#849b87']}
                        />) :
                            (filteredCountries.length > 0 ? (
                                filteredCountries.map((country) => (
                                    <DisplayPage key={country.name.common} countryData={country} />
                                ))) : <div className='fs-1 text-center'>No matching countries found</div>)}
                    </div>
                </div>
            </div>
        </>
    )
}